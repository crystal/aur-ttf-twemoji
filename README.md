# ttf-twemoji
This is a fork of https://aur.archlinux.org/packages/ttf-twemoji, I just edited the PKGBUILD so it downloads Twemoji 13.1.0 from the Fedora packages, since the AUR version is out of date (as of this writing).

### Installation
Download the repository via git and build/install with makepkg

```
git clone https://codeberg.org/crystal/aur-ttf-twemoji
cd aur-ttf-twemoji
makepkg -si
```

